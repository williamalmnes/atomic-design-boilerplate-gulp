// > define dependencies
define([
	'vendor/handlebars',
	'moment'
], function(
	Handlebars,
	Moment
){

	function Helpers(){
		this.init();
	}

	Helpers.prototype.init = function(){

		Handlebars.registerHelper('ifIn', function(elem, list, options) {
			if(list.indexOf(elem) > -1) {
				return options.fn(this);
			}
			return options.inverse(this);
		});

		Handlebars.registerHelper('ifHas', function(arr, property, value, options) {
			for (var i = 0; i < arr.length; i++) {
				if(arr[i][property] === value){
					return options.fn(this);
				}
			}
			return options.inverse(this);
		});

		Handlebars.registerHelper('ifExists', function(val, list, property, options) {
			var self = this;
			var match = false;

			if( ! list || list.length === 0){ return options.inverse(self); }

			list.forEach(function(element){
				if(element[property] === val){
					match = true;
				}
			});

			if(match){ return options.fn(self); }
			return options.inverse(self);
		});

		Handlebars.registerHelper('join', function(list, property, separator) {
			var self = this;
			var arr = [];

			list.forEach(function(passenger){
				arr.push(passenger[property]);
			});

			return arr.join(separator);
		});

		Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
		    switch (operator) {
		        case '==':
		            return (v1 === v2) ? options.fn(this) : options.inverse(this);
		        case '!=':
		            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
		        default:
		            return options.inverse(this);
		    }
		});
		
		Handlebars.registerHelper('productId', function(obj, type) {
			return obj[type+'Id'];
		});

		Handlebars.registerHelper('dateRange', function(dates, options) {
			console.log(dates);
		});

		Handlebars.registerHelper('printSelectedPassengers', function(list, separator, suffix, options) {
			var arr = [];
			list.forEach(function(passenger){
				if(passenger.isSelected){ arr.push(passenger.displayName); }
			});
			if(arr.length === 0){ return null; }
			return new Handlebars.SafeString('<p class="booking-addon-selected-passengers">'+ arr.join(separator) +' ' + suffix +'</p>');
		});

		Handlebars.registerHelper("formatDate", function(date, options) {
			var moment = new Moment(date);
		  	return moment.date();
		});
	};

	// > return the component
	return Helpers;
});
