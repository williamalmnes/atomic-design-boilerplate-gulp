
/**
 * Handlebars Helper for Pattern Library
 *
 * Copyright (c) 2014 Yonas Sandbæk
 * Licensed under the MIT License (MIT).
 */

module.exports.register = function (Handlebars, options, params) {
	'use strict';

	Handlebars.registerPartial("javascriptImports", "");

	Handlebars.registerHelper("import-scripts", function(options) {
		Handlebars.registerPartial("javascriptImports", options.fn(this));
	});
};
