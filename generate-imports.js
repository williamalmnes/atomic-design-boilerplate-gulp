var through = require('through2');
var gutil = require('gulp-util');
var fs = require("fs");
var path = require("path");
var config = require("./config");


module.exports = function(outname){
 var paths = [];  // where we will push the path names with the @import

 var write = function (file, enc, cb){
   if (file.path != "undefined"){
     var basepath = process.cwd() + config.path.src.substring(1),
       filename = path.basename(file.path),
       fullpath = file.path.substring(basepath.length).replace(filename, filename.substring(1).replace(".scss","")),
       uniformpath = fullpath.replace(/\\/g, '/'), //Fix for windows style paths
       statement = '@import "../' + uniformpath + '";';

     paths.push(statement);
   }
   cb();
 };

 var flush = function(cb){  // flush occurs at the end of the concating from write()
   // gutil.log(gutil.colors.cyan(paths));  // log it

   paths.sort(function(a,b){
     if(a < b){
       return -1;
     }else if(a > b){
       return 1;
     }
     return 0;
   });

   // Write to file
   fs.writeFileSync(config.paths.styles.src  + '_components.scss', '// Do not edit. Auto generated.\n' + paths.join("\n"));

   cb();
 };

 return through.obj(write, flush);  // return it
};
