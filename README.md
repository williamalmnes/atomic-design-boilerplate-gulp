Atomic Design Boilerplate Gulp edition
=====================
> Yonas Sandbaek [@seltar](https://twitter.com/seltar), [github](https://github.com/seltar), [bitbucket](https://bitbucket.org/seltar)  
> https://bitbucket.org/seltar/atomic-design-boilerplate-gulp

###### Version
> 0.2.9

### What is this
This project aims to give structure and improve automation when working with atomic design principles.

### What does it do?
- Separates between development and production.
- Compiles handlebars templates with partials, helpers and external data to html
- Compiles, autoprefixes and minifies sass files to css
- Lints, compiles and minifies js with requirejs optimizer

### Known issues
- gulp.watch doesn't react to newly added files. Rerun gulp as a fix for now. 

### Starting up
- Clone the [repo](https://seltar@bitbucket.org/seltar/atomic-design-boilerplate-gulp.git)
- Open up terminal and install dependencies with `[sudo] npm install`
- Run `gulp` to build and start watching
- Go forth and write your jargon, you little codemonkey!

### Configuration
- To change the folder configuration or site information, edit `config.js`


### Building
Here are the tasks you can use with gulp


- Start server and watch for changes
	
		gulp server

- Clean folders

		gulp clean

- Build html

		gulp html

- Lint and compile javascript

		gulp scripts 

- Generate SASS import for components and compile SASS

		gulp styles 

- Build html, compile javascript, compile sass, start server and watch for changes

		gulp

- Add a `--production` flag to run production tasks

		i.e. gulp --production

---

### Folder structure
	root
	│
	├───.gitignore
	├───config.js
	├───gulpfile.js
	├───generate-imports.js
	├───package.json
	├───README.md
	│
	├───app
	│   │
	│   ├───assets
	│   │   │   
	│   │   ├───ajax
	│   │   │   
	│   │   ├───dummy-images
	│   │   │   
	│   │   ├───fonts
	│   │   │   
	│   │   └───images
	│   │       
	│	│───components
	│	│   │
	│	│   ├───_helpers
	│	│   │   │   
	│	│   │   │   # all .js files in this folder are included as handlebars helpers.
	│	│   │   └───atomic.js
	│	│   │
	│	│   ├───_layouts
	│	│   │   │
	│	│   │   │   # main layouts
	│	│   │   └───default.hbs
	│	│   │       
	│	│   ├───_partials
	│	│   │   │
	│	│   │   │   # all .hbs files in this folder are included as handlebars partials.
	│	│   │   ├───head.hbs
	│	│   │   └───javascripts.hbs
	│	│   │       
	│	│   ├───abstract
	│	│   │   │
	│	│   │   │   # Abstract components
	│	│   │       
	│	│   ├───inline-templates
	│	│   │   │
	│	│   │   │   # Templates used in the live app
	│	│   │       
	│	│   ├───plugins
	│	│   │   │
	│	│   │   │   # Javascript plugins
	│	│   │       
	│	│   ├───atoms
	│	│   │   │
	│	│   │   │   # Atoms
	│	│   │   └───example
	│	│   │       │   # styles
	│	│   │       ├───_example.scss
	│	│   │       │   # template
	│	│   │       ├───example.hbs
	│	│   │       │   # script
	│	│   │       ├───example.js
	│	│   │       │   # unit test
	│	│   │       └───example.test.js
	│	│   │       
	│	│   ├───molecules
	│	│   │   │
	│	│   │   │   # Molecules
	│	│   │       
	│	│   ├───organisms
	│	│   │   │
	│	│   │   │   # Organisms
	│	│   │       
	│	│   └───templates
	│	│       │
	│	│       │   # Templates
	│   │       
	│   ├───data
	│   │       
	│   ├───js
	│   │   │   
	│   │   └───vendor
	│   │       
	│   └───sass
	│           
	│           
	└─node_modules